const calendarTheme = document.querySelector("#theme").innerHTML;
const calendarThemeColor = window.getComputedStyle( document.querySelector("#theme"),null).getPropertyValue('background-color'); 
const calendarButton = document.querySelector(".btn-start");
const calendarContainer = document.querySelector(".container");
const calendarDays = 24;

const openDoor = (path, event) => {
    event.target.parentNode.style.backgroundImage = `url(${path})`;
    event.target.style.opacity = "0";
    event.target.style.backgroundColor = calendarThemeColor;
}

const createCalendar = () => {    
    if(calendarButton.style.opacity == '0'){
        //if button has opacity 0
        console.log("don't restart");
    }else{            
        for(let i = 0; i  < calendarDays; i++) {
            const calendarDoor = document.createElement("div");
            const calendarDoorText = document.createElement("div");

            calendarDoor.classList.add("image");
            calendarDoor.style.gridArea = "door" + (i + 1);
            calendarContainer.appendChild(calendarDoor);

            calendarDoorText.classList.add("text");
            calendarDoorText.innerHTML = i + 1;
            calendarDoor.appendChild(calendarDoorText);

            doorNumber = i + 1;
            let doorPath = `./themes/`+calendarTheme+`/assets/door-${doorNumber}.jpg`;

            calendarDoorText.addEventListener("click", openDoor.bind(null,  doorPath));
            calendarButton.style.opacity = "0";
        }
    }
}

calendarButton.addEventListener("click", createCalendar);

