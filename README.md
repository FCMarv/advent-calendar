# advent calendar
Advent calendar made with php, css and js

replace example_theme assets by the pictures you want to see

## Load the advent calendar using a theme from url
1. Replace `<theme>` with the name of your theme.
  * `http://localhost:8000//advent-calendar/<theme>`
  * `http://www.example.com/<theme>`
  * `http://www.example.com/advent-calendar/<theme>`