<!DOCTYPE html>
<?php
    //get current file location of index.php
    if(preg_match("/.*(\\\\|.*\/)(.*)/",getcwd(),$matches)){
        $fileDir=$matches[2];
    }
    //Check which theme is used
    $url= $_SERVER['REQUEST_URI'];
    if(preg_match("/\/($fileDir)\/(.*)/",$url,$matches))
    {
        //When people use http://examle.nl/advent-calendar/[theme]
        $theme=$matches[2];
        if($theme == ""){     
            $theme="default";
        }
    }
    elseif(preg_match("/\/(.*)/",$url,$matches)){
        //When people use http://example.nl/[theme]
        $theme=$matches[1];
        if($theme == ""){     
            $theme="default";
        }
    }
    else
    {   
        //when nothing is entered behind domain use default theme
        $theme="default";
    }  

?>
<html lang="en">
  <head>
    <META NAME="robots" CONTENT="noindex">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    
    <link rel="stylesheet" href="css/styles.css" />
    <link rel="stylesheet" href="themes/<?=$theme?>/css/styles.css" />
    <title>Advent calendar</title>
  </head>
  <body>
    <div id="theme"><?=$theme?></div>
    <button class="btn-start">Create Calendar</button>
    <div class="container">
    <script src="main.js"></script>
  </body>
</html>
